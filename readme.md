# OpenLayers / Custom controls

En este ejemplo se usa la clase `ol/control/Rotate`, un control de botón para restablecer la rotación a 0. Además permite a los usuarios dibujar, modificar y seleccionar formas geométricas en un mapa.

Primero se importan estilos CSS y las clases necesarias de _*OL*_, incluyendo clases para la creación de mapas (`Map`, `View`), interacciones (`Draw`, `Modify`, `Select`, `Snap`), fuentes de datos (`OSM`, `VectorSource`) y capas (`TileLayer`, `VectorLayer`), así como controles (`Control`, `defaultControls`).

Ahora se creará una capa base raster utilizando `TileLayer` con _*OpenStreetMap*_ (OSM) como fuente. Y una capa vectorial con `VectorLayer` usando `VectorSource` como fuente, y se define un estilo para las geometrías dibujadas, incluyendo color de relleno, borde, y configuraciones para círculos.

Se define una clase `RotateNorthControl`  extendiendo `Control` para crear un botón personalizado que, al ser presionado, rota el mapa hacia el norte (rotación = 0).

```js
class RotateNorthControl extends Control {
    constructor(opt_options) {
    const options = opt_options || {};
    . 
  }

  handleRotateNorth() {
    this.getMap().getView().setRotation(0);
  }
}
```

Ahora se crea el mapa con las capas raster y vectorial añadidas, el control personalizado para rotar hacia el norte, y una View inicial que define el centro, el zoom y la rotación del mapa.

Se definen dos objetos `ExampleModify`: que inicializa las interacciones de selección (Select) y modificación (Modify) de las características vectoriales, permitiendo a los usuarios modificar las formas dibujadas. Y `ExampleDraw` para gestionar las interacciones de dibujo (Draw) de varios tipos de geometrías (punto, línea, polígono, círculo), permitiendo a los usuarios añadir nuevas formas al mapa. 

Se agregan eventos para activar o desactivar las interacciones de dibujo o modificación basándose en la entrada del usuario, permitiendo cambiar entre modos de interacción.

Finalmente se añade una interacción `Snap` para facilitar el dibujo y la modificación de geometrías al hacer que los puntos se adhieran a las geometrías existentes.

